/*
  Проект уличного освещения на attiny13
  buttonOn при первом нажатии включает лампу1, при повторных нажатиях включает или отключает лампу2
  buttonOff выключает обы лампы
*/

#include <EEPROM.h>

#define ldr A1       // PB2=pin7 (VCC-LDR-ADC1-10K-GND)
#define ldr_addr 0  // адрес порога выключения света в EEPROM
#define buttonOff 0 // PB0=pin5
#define buttonOn  1 // PB1=pin6
#define led1  3     // PB3=pin2
#define led2  4     // PB4=pin3
#define buttonMaxCount 500  // количество циклов антидребезга
#define setupMaxCount 150000 // количество циклов до входа в SETUP
#define blinkMaxCount 30000  // количество циклов для мигания светодиодами
#define idleMaxCount 1000000  // количество циклов бездействия

#define WORK true
#define SETUP false

boolean mode = WORK; // режим работы TRUE - WORK, FALSE - SETUP
byte ledState = 0;    // номер режима
word blinkCount = 0; // счётчик мигания
long idleCount = 0;  // счётчик простоя

boolean buttonOnState;
boolean pressedOn;
word buttonOnCount; // счётчик антидребезга
boolean buttonOffState;
boolean pressedOff;
long buttonOffCount; // счётчик антидребезга

byte light;           // текущий уровень освещённости
byte level;           // порог отключения освещения

void setup() {
  // настройка порта фоторезистора
  analogReference(DEFAULT);
  pinMode(ldr, INPUT);
  // initialize the LED pin as an output:
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonOn, INPUT);
  digitalWrite(buttonOn, HIGH); //pullup
  pinMode(buttonOff, INPUT);
  digitalWrite(buttonOff, HIGH); //pullup
  // читаем EEPROM
  level = EEPROM.read(ldr_addr);
  if (level == 255) mode = SETUP; //если не установлен порог освещённости, то сразу в SETUP
}

void loop() {
  //============================================================
  if (mode) WorkMode(); else SetupMode();
  //============================================================
  switch (ledState) {
    case 0:
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      break;
    case 1:
      digitalWrite(led1, HIGH);
      digitalWrite(led2, LOW);
      break;
    case 2:
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      break;
    default:
      break;
  }
  //============================================================
}

void WorkMode() {
  readButtons();
  //============================================================
  if (buttonOnState == LOW) {
    if (buttonOnCount < buttonMaxCount) buttonOnCount++;
    else {
      if (!pressedOn) {
        pressedOn = true;
        switch (ledState) {
          case 1:
            ledState = 2;
            break;
          default:
            ledState = 1;
            break;
        }
      }
    }
  }
  //============================================================
  if (buttonOffState == LOW) {
    ledState = 0;
    if (buttonOffCount < setupMaxCount) buttonOffCount++;
    else {
      if (!pressedOff) {
        pressedOff = true;
        mode = SETUP;
        ledState = 2;
      }
    }
  }
  //============================================================
  light = analogRead(ldr) >> 2;
  if (light > level) ledState = 0;
  //============================================================
}

void SetupMode() {
  //============================================================
  readButtons();
  idleCount++;
  //============================================================
  if (blinkCount < blinkMaxCount) blinkCount++; else {
    blinkCount = 0;
    if (ledState == 0) ledState = 2; else ledState = 0;
  }
  //============================================================
  if (buttonOnState == LOW) {
    idleCount = 0;
    if (buttonOnCount < buttonMaxCount) buttonOnCount++;
    else {
      if (!pressedOn) {
        pressedOn = true;
        ledState = 1;
        blinkCount = 0;
        if (level < 240) level += 10;
      }
    }
  }
  //============================================================
  if (buttonOffState == LOW) {
    idleCount = 0;
    if (buttonOffCount < buttonMaxCount) buttonOffCount++;
    else {
      if (!pressedOff) {
        pressedOff = true;
        ledState = 1;
        blinkCount = 0;
        if (level > 10) level -= 10;
      }
    }
  }
  //============================================================
  if (idleCount > idleMaxCount) {
    ledState = 0;
    idleCount = 0;
    mode = WORK;
    EEPROM.write(ldr_addr, level);
  }
  //============================================================
}

void readButtons() {
  buttonOnState = digitalRead(buttonOn);
  if (buttonOnState == HIGH) {
    buttonOnCount = 0;
    pressedOn = false;
  }
  buttonOffState = digitalRead(buttonOff);
  if (buttonOffState == HIGH) {
    buttonOffCount = 0;
    pressedOff = false;
  }
}

